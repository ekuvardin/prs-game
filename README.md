# PRS-Game

## Dependency

To successfully build and run project you will need

    1. maven 3.5.0
    2. Java 8+

---

## Build

Next step will help you to run project

1. After clone repo/extract archive in commandline navigate to main folder
2. Run "mvn clean install"

How to run

    1. Using executable jar
        In the command line
            1.1 cd <Project folder>
            1.2 java -jar target/prs-game-1.0.jar

    2. maven-exec-plugin
        In the command line
            2.1 cd <Project folder>
            2.2 mvn exec:java

---

## Available commands

    !q - exit program

    You will see
        example
            !q
            The program will be terminated

    R(ROCK), S(SCISSORS), P(PAPER) - type of pick to play with computer
        example
            ROCK
            Computer plays SCISSORS, you plays ROCK
            Player wins

---

## Project structure

    Main class
        Finder\src\main\java\runner\SimplePRCGame

    Integration tests
        \src\test\integration

    Unit tests
        \src\test\unit
