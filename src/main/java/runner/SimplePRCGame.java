package runner;


import command.ICommand;
import command.QuitCommand;
import game.RandomStrategy;
import parser.IConsoleParser;
import parser.OptionParserImpl;
import view.ConsoleViewer;

import java.util.Random;
import java.util.Scanner;

/**
 * Main class. Start program.
 */
public class SimplePRCGame {

    public static void main(String[] args) {
        IConsoleParser parser = new OptionParserImpl(new RandomStrategy(new Random()));
        ConsoleViewer view = new ConsoleViewer();
        view.printMessage("Welcome to a simple Paper-Rock-Scissors game!");
        view.printMessage("Please type R(ROCK), S(SCISSORS), P(PAPER) to play with computer");

        try (Scanner scanner = new Scanner(System.in)) {
            ICommand command;
            do {
                command = parser.parseCommand((scanner.nextLine()).split("\\s+"));
                command.execute(view);
            } while (!(command instanceof QuitCommand));
        }
    }
}