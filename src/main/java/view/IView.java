package view;

/**
 * Display commands
 */
public interface IView {

    /**
     * Display message error
     *
     * @param message message error
     */
    void showError(String message);

    /**
     * Print message
     *
     * @param message message to print
     */
    void printMessage(String message);
}
