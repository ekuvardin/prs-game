package view;

import java.util.Arrays;

/**
 * Display commands to user
 */
public class ConsoleViewer implements IView {

    @Override
    public void showError(String message) {
        System.out.println("ERROR: " + message);
        System.out.println();
    }

    @Override
    public void printMessage(String message) {
        System.out.println(message);
    }
}

