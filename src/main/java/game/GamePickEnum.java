package game;

import org.jetbrains.annotations.NotNull;

/**
 * Possible value for game
 */
public enum GamePickEnum {

    ROCK(0) {
        @Override
        public String getShortName() {
            return "R";
        }
    },
    PAPER(1) {
        @Override
        public String getShortName() {
            return "P";
        }
    },
    SCISSORS(2) {
        @Override
        public String getShortName() {
            return "S";
        }
    };

    GamePickEnum(int index) {
        this.index = index;
    }

    public enum GameResultEnum {
        WIN,
        LOOSE,
        STANDOFF;

        public GameResultEnum opposite() {
            return this == WIN ? LOOSE : this == LOOSE ? WIN : STANDOFF;
        }
    }

    private final int index;
    private GameResultEnum matrix[][];

    protected GameResultEnum[][] getMatrix() {
        if (matrix == null) {
            int length = GamePickEnum.values().length;
            matrix = new GameResultEnum[length][length];

            matrix[ROCK.index][PAPER.index] = GameResultEnum.LOOSE;
            matrix[ROCK.index][SCISSORS.index] = GameResultEnum.WIN;
            matrix[PAPER.index][SCISSORS.index] = GameResultEnum.LOOSE;

            for (int i = 0; i < length; i++)
                for (int j = 0; j <= i; j++)
                    matrix[i][j] = i == j ? GameResultEnum.STANDOFF : matrix[j][i].opposite();
        }

        return matrix;
    }

    /**
     * Choose winner from 2 players
     *
     * @param pickPlayer player to whom current user player
     * @return -1 - first player wins
     * 0 - standoff
     * 1 - second player wins
     */
    public GameResultEnum defineWinner(@NotNull GamePickEnum pickPlayer) {
        return getMatrix()[this.index][pickPlayer.index];
    }

    public abstract String getShortName();

    public static GamePickEnum findType(String value) {
        for (GamePickEnum entry : GamePickEnum.values()) {
            if (entry.toString().equalsIgnoreCase(value) || entry.getShortName().equalsIgnoreCase(value))
                return entry;
        }
        return null;
    }
}
