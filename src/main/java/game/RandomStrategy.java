package game;

import java.util.Random;

public class RandomStrategy implements IStrategy {

    private final Random random;

    public RandomStrategy(Random random) {
        this.random = random;
    }

    @Override
    public GamePickEnum play() {
        int result = random.nextInt(3) + 1;
        switch (result) {
            case 1:
                return GamePickEnum.PAPER;
            case 2:
                return GamePickEnum.ROCK;
            default:
                return GamePickEnum.SCISSORS;
        }
    }
}