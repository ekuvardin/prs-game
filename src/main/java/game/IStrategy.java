package game;

public interface IStrategy {

    GamePickEnum play();
}
