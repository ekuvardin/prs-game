package command;

import view.IView;

/**
 * Contains error during parsing command
 */
public class MessageCommand implements ICommand {

    private String message;

    public String getMessage() {
        return message;
    }

    public MessageCommand(String message) {
        this.message = message;
    }

    @Override
    public void execute(IView view) {
        view.showError(message);
    }
}