package command;


import org.jetbrains.annotations.NotNull;
import view.IView;

/*
   Last command before terminating
 */
public class QuitCommand implements ICommand {

    public void execute(@NotNull IView view) {
        view.printMessage("The program will be terminated");
    }

}