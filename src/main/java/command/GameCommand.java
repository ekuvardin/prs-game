package command;

import game.GamePickEnum;
import game.IStrategy;
import game.RandomStrategy;
import org.jetbrains.annotations.NotNull;
import view.IView;

/**
 * Play game with computer
 */
public class GameCommand implements ICommand {

    private final GamePickEnum userRound;
    private final IStrategy strategy;

    public GameCommand(@NotNull GamePickEnum userRound, @NotNull IStrategy strategy) {
        this.userRound = userRound;
        this.strategy = strategy;
    }

    @Override
    public void execute(@NotNull IView view) {
        GamePickEnum pcRound = strategy.play();
        GamePickEnum.GameResultEnum res = pcRound.defineWinner(userRound);

        view.printMessage(String.format("Computer plays %s, you plays %s", pcRound, userRound));
        view.printMessage(res == GamePickEnum.GameResultEnum.STANDOFF ? "Standoff" : res == GamePickEnum.GameResultEnum.WIN ? "Computer wins" : "Player wins");
        view.printMessage("");
    }

    public GamePickEnum getUserRound() {
        return userRound;
    }
}