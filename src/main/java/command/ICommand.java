package command;

import view.IView;

/*
  Define command to interact with user
 */
public interface ICommand {

    /**
     * Execute command
     *
     * @param view where to show results
     */
    void execute(IView view);
}