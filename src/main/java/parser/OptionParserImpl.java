package parser;

import command.MessageCommand;
import command.GameCommand;
import command.ICommand;
import command.QuitCommand;
import game.GamePickEnum;
import game.IStrategy;

public class OptionParserImpl implements IConsoleParser {

    private final IStrategy strategy;

    public OptionParserImpl(IStrategy strategy) {
        this.strategy = strategy;
    }

    @Override
    public ICommand parseCommand(String[] args) {
        if (args == null || args.length <= 0)
            return new MessageCommand("Empty command");

        if (args.length > 1)
            return new MessageCommand(String.format("Unknown command %s", args[1]));

        if ("!q".equals(args[0]))
            return new QuitCommand();

        return parseUserInput(args[0]);
    }

    private ICommand parseUserInput(String arg) {
        GamePickEnum result = GamePickEnum.findType(arg);
        if (result == null)
            return new MessageCommand(String.format("Unknown command %s", arg));

        return new GameCommand(result, strategy);
    }
}
