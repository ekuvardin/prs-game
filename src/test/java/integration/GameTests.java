package integration;

import command.MessageCommand;
import command.GameCommand;
import command.ICommand;
import command.QuitCommand;
import game.GamePickEnum;
import game.IStrategy;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import view.ConsoleViewer;

import java.io.*;

public class GameTests {

    private PrintStream oldOut;

    ConsoleViewer view = new ConsoleViewer();

    @Before
    public void preTest() {
        oldOut = System.out;
    }

    @After
    public void afterTest() {
        System.setOut(oldOut);
    }

    @Test
    public void immediateQuitShouldViewMessage() throws IOException {
        QuitCommand command = new QuitCommand();
        checkResult(command, "The program will be terminated" + "\r\n");
    }

    @Test
    public void immediateErrorCommandViewMessage() throws IOException {
        MessageCommand command = new MessageCommand("test");
        checkResult(command, "ERROR: test" + "\r\n" + "\r\n");
    }

    @Test
    public void immediateGameCommandPaperPaperShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.PAPER);
        GameCommand command = new GameCommand(GamePickEnum.PAPER, strategy);

        checkResult(command, "Computer plays PAPER, you plays PAPER" + "\r\n" +
                "Standoff" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandPaperRockShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.PAPER);
        GameCommand command = new GameCommand(GamePickEnum.ROCK, strategy);

        checkResult(command, "Computer plays PAPER, you plays ROCK" + "\r\n" +
                "Computer wins" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandPaperScissorsShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.PAPER);
        GameCommand command = new GameCommand(GamePickEnum.SCISSORS, strategy);

        checkResult(command, "Computer plays PAPER, you plays SCISSORS" + "\r\n" +
                "Player wins" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandRockPaperShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.ROCK);
        GameCommand command = new GameCommand(GamePickEnum.PAPER, strategy);

        checkResult(command, "Computer plays ROCK, you plays PAPER" + "\r\n" +
                "Player wins" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandRockRockShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.ROCK);
        GameCommand command = new GameCommand(GamePickEnum.ROCK, strategy);

        checkResult(command, "Computer plays ROCK, you plays ROCK" + "\r\n" +
                "Standoff" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandRockScissorsShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.ROCK);
        GameCommand command = new GameCommand(GamePickEnum.SCISSORS, strategy);

        checkResult(command, "Computer plays ROCK, you plays SCISSORS" + "\r\n" +
                "Computer wins" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandScissorsPaperShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.SCISSORS);
        GameCommand command = new GameCommand(GamePickEnum.PAPER, strategy);

        checkResult(command, "Computer plays SCISSORS, you plays PAPER" + "\r\n" +
                "Computer wins" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandScissorsRockShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.SCISSORS);
        GameCommand command = new GameCommand(GamePickEnum.ROCK, strategy);

        checkResult(command, "Computer plays SCISSORS, you plays ROCK" + "\r\n" +
                "Player wins" + "\r\n"+ "\r\n");
    }

    @Test
    public void immediateGameCommandScissorsScissorsShouldViewMessage() throws IOException {
        TestStrategy strategy = new TestStrategy();
        strategy.setReturn(GamePickEnum.SCISSORS);
        GameCommand command = new GameCommand(GamePickEnum.SCISSORS, strategy);

        checkResult(command, "Computer plays SCISSORS, you plays SCISSORS" + "\r\n" +
                "Standoff" + "\r\n"+ "\r\n");
    }

    protected void checkResult(ICommand command, String expected) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try (PrintStream ps = new PrintStream(baos)) {
            System.setOut(ps);

            command.execute(view);

            Assert.assertEquals(expected, baos.toString());

            System.out.flush();
            baos.reset();
        } finally {
            baos.close();
        }
    }

    private class TestStrategy implements IStrategy {

        private GamePickEnum result;

        public void setReturn(GamePickEnum result) {
            this.result = result;
        }

        @Override
        public GamePickEnum play() {
            return this.result;
        }
    }

}