package junit.parser;

import command.MessageCommand;
import command.GameCommand;
import command.ICommand;
import command.QuitCommand;
import game.GamePickEnum;
import game.IStrategy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import parser.OptionParserImpl;

public class OptionParserImplTest {

    @Mock
    IStrategy strategy;

    OptionParserImpl optionParserImpl;

    @Before
    public void preTest(){
        MockitoAnnotations.initMocks(this);
        optionParserImpl = new OptionParserImpl(strategy);
    }

    @Test
    public void parseCommandShouldReturnErrorCommandWhenArgsIsEmpty(){
        ICommand command = optionParserImpl.parseCommand(new String[0]);

        Assert.assertTrue(command instanceof MessageCommand);
        Assert.assertEquals("Empty command", ((MessageCommand)command).getMessage());
    }

    @Test
    public void parseCommandShouldReturnErrorCommandWhenArgsIsNull(){
        ICommand command = optionParserImpl.parseCommand(null);

        Assert.assertTrue(command instanceof MessageCommand);
        Assert.assertEquals("Empty command", ((MessageCommand)command).getMessage());
    }

    @Test
    public void parseCommandShouldReturnErrorCommandWhenArgsIsTooBig(){
        ICommand command = optionParserImpl.parseCommand(new String[]{"s","s"});

        Assert.assertTrue(command instanceof MessageCommand);
        Assert.assertEquals("Unknown command s", ((MessageCommand)command).getMessage());
    }

    @Test
    public void parseCommandShouldReturnQuitCommand(){
        ICommand command = optionParserImpl.parseCommand(new String[]{"!q"});

        Assert.assertTrue(command instanceof QuitCommand);
    }

    @Test
    public void parseCommandShouldReturnErrorCommandWhenArgsNotKnown(){
        ICommand command = optionParserImpl.parseCommand(new String[]{"w"});

        Assert.assertTrue(command instanceof MessageCommand);
        Assert.assertEquals("Unknown command w", ((MessageCommand)command).getMessage());
    }

    @Test
    public void parseCommandShouldReturnRockGameCommand(){
        ICommand command = optionParserImpl.parseCommand(new String[]{"r"});

        Assert.assertTrue(command instanceof GameCommand);
        Assert.assertEquals(GamePickEnum.ROCK, ((GameCommand)command).getUserRound());
    }

    @Test
    public void parseCommandShouldReturnPaperGameCommand(){
        ICommand command = optionParserImpl.parseCommand(new String[]{"p"});

        Assert.assertTrue(command instanceof GameCommand);
        Assert.assertEquals(GamePickEnum.PAPER, ((GameCommand)command).getUserRound());
    }

    @Test
    public void parseCommandShouldReturnScissorsGameCommand(){
        ICommand command = optionParserImpl.parseCommand(new String[]{"s"});

        Assert.assertTrue(command instanceof GameCommand);
        Assert.assertEquals(GamePickEnum.SCISSORS, ((GameCommand)command).getUserRound());
    }
}
