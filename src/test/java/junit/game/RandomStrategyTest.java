package junit.game;

import game.GamePickEnum;
import game.RandomStrategy;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.Random;

public class RandomStrategyTest {

    @Mock
    Random random;

    RandomStrategy randomStrategy;

    @Before
    public void preTest(){
        MockitoAnnotations.initMocks(this);
        randomStrategy = new RandomStrategy(random);
    }

    @Test
    public void playShouldCallRandomNextInt(){
        randomStrategy.play();

        Mockito.verify(random).nextInt(3);
    }

    @Test
    public void playShouldReturnPaper(){
        Mockito.when(random.nextInt(3)).thenReturn(0);

        Assert.assertEquals(GamePickEnum.PAPER, randomStrategy.play());
    }

    @Test
    public void playShouldReturnRock(){
        Mockito.when(random.nextInt(3)).thenReturn(1);

        Assert.assertEquals(GamePickEnum.ROCK, randomStrategy.play());
    }

    @Test
    public void playShouldReturnScissors(){
        Mockito.when(random.nextInt(3)).thenReturn(2);

        Assert.assertEquals(GamePickEnum.SCISSORS, randomStrategy.play());
    }
}
