package junit.game;

import game.GamePickEnum;
import org.junit.Assert;
import org.junit.Test;

public class GamePickEnumTest {

    @Test
    public void defineWinnerShouldReturnOneWhenRockScissor(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.WIN, GamePickEnum.ROCK.defineWinner(GamePickEnum.SCISSORS));
    }

    @Test
    public void defineWinnerShouldReturnZeroWhenRockRock(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.STANDOFF, GamePickEnum.ROCK.defineWinner(GamePickEnum.ROCK));
    }

    @Test
    public void defineWinnerShouldReturnMinusOneWhenRockPaper(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.LOOSE, GamePickEnum.ROCK.defineWinner(GamePickEnum.PAPER));
    }

    @Test
    public void defineWinnerShouldReturnOneWhenScissorPaper(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.WIN, GamePickEnum.SCISSORS.defineWinner(GamePickEnum.PAPER));
    }

    @Test
    public void defineWinnerShouldReturnOneWhenScissorScissor(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.STANDOFF, GamePickEnum.SCISSORS.defineWinner(GamePickEnum.SCISSORS));
    }

    @Test
    public void defineWinnerShouldReturnOneWhenScissorRock(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.LOOSE, GamePickEnum.SCISSORS.defineWinner(GamePickEnum.ROCK));
    }

    @Test
    public void defineWinnerShouldReturnOneWhenPaperRock(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.WIN, GamePickEnum.PAPER.defineWinner(GamePickEnum.ROCK));
    }

    @Test
    public void defineWinnerShouldReturnOneWhenPaperPaper(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.STANDOFF, GamePickEnum.PAPER.defineWinner(GamePickEnum.PAPER));
    }

    @Test
    public void defineWinnerShouldReturnOneWhenPaperScissor(){
        Assert.assertEquals(GamePickEnum.GameResultEnum.LOOSE, GamePickEnum.PAPER.defineWinner(GamePickEnum.SCISSORS));
    }

    @Test
    public void findTypeShouldReturnRock(){
        Assert.assertEquals(GamePickEnum.ROCK, GamePickEnum.findType("ROCK"));
    }

    @Test
    public void findTypeShouldReturnRockWhenGetLowerCase(){
        Assert.assertEquals(GamePickEnum.ROCK, GamePickEnum.findType("rock"));
    }

    @Test
    public void findTypeShouldReturnRockWhenGetShortName(){
        Assert.assertEquals(GamePickEnum.ROCK, GamePickEnum.findType("R"));
    }

    @Test
    public void findTypeShouldReturnRockWhenGetLowerCaseShortName(){
        Assert.assertEquals(GamePickEnum.ROCK, GamePickEnum.findType("r"));
    }

    @Test
    public void findTypeShouldReturnPaper(){
        Assert.assertEquals(GamePickEnum.PAPER, GamePickEnum.findType("PAPER"));
    }

    @Test
    public void findTypeShouldReturnPaperWhenGetLowerCase(){
        Assert.assertEquals(GamePickEnum.PAPER, GamePickEnum.findType("paper"));
    }

    @Test
    public void findTypeShouldReturnPaperWhenGetShortName(){
        Assert.assertEquals(GamePickEnum.PAPER, GamePickEnum.findType("P"));
    }

    @Test
    public void findTypeShouldReturnPaperWhenGetLowerCaseShortName(){
        Assert.assertEquals(GamePickEnum.PAPER, GamePickEnum.findType("p"));
    }

    @Test
    public void findTypeShouldReturnScissors(){
        Assert.assertEquals(GamePickEnum.SCISSORS, GamePickEnum.findType("SCISSORS"));
    }

    @Test
    public void findTypeShouldReturnScissorsWhenGetLowerCase(){
        Assert.assertEquals(GamePickEnum.SCISSORS, GamePickEnum.findType("scissors"));
    }

    @Test
    public void findTypeShouldReturnScissorsWhenGetShortName(){
        Assert.assertEquals(GamePickEnum.SCISSORS, GamePickEnum.findType("S"));
    }

    @Test
    public void findTypeShouldReturnScissorsWhenGetLowerCaseShortName(){
        Assert.assertEquals(GamePickEnum.SCISSORS, GamePickEnum.findType("s"));
    }

    @Test
    public void findTypeShouldReturnNull(){
        Assert.assertNull(GamePickEnum.findType("ee"));
    }
}
