package junit.command;

import command.MessageCommand;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import view.IView;

public class MessageCommandTest {

    @Mock
    IView view;

    MessageCommand messageCommand;

    String message = "test";

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        messageCommand = new MessageCommand(message);
    }

    @Test
    public void executeShouldCallPrintMessage(){
        messageCommand.execute(view);

        Mockito.verify(view).showError(message);
    }
}
