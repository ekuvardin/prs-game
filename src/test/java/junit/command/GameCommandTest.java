package junit.command;

import command.GameCommand;
import game.GamePickEnum;
import game.IStrategy;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import view.IView;

public class GameCommandTest {

    @Mock
    IView view;

    GameCommand gameCommand;

    @Mock
    GamePickEnum pickPc;

    @Mock
    GamePickEnum pickPlayer;

    @Mock
    IStrategy strategy;

    String pickPcName = "pickPc";
    String pickPlayerName = "pickPlayer";

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        gameCommand = new GameCommand(pickPlayer, strategy);

        Mockito.when(strategy.play()).thenReturn(pickPc);
        Mockito.when(pickPc.defineWinner(pickPlayer)).thenReturn(GamePickEnum.GameResultEnum.STANDOFF);

        Mockito.when(pickPc.toString()).thenReturn(pickPcName);
        Mockito.when(pickPlayer.toString()).thenReturn(pickPlayerName);
    }

    @Test
    public void executeShouldCallStrategyPlay(){
        gameCommand.execute(view);

        Mockito.verify(strategy).play();
    }

    @Test
    public void executeShouldCallDefineWinner(){
        gameCommand.execute(view);

        Mockito.verify(pickPc).defineWinner(pickPlayer);
    }

    @Test
    public void executeShouldCallViewPrintMessageWithPlayerPicks(){
        gameCommand.execute(view);

        Mockito.verify(view).printMessage(String.format("Computer plays %s, you plays %s", pickPcName, pickPlayerName));
    }

    @Test
    public void executeShouldCallViewPrintStandoffMessage(){
        gameCommand.execute(view);

        Mockito.verify(view).printMessage("Standoff");
    }

    @Test
    public void executeShouldCallViewPrintComputerWinsMessage(){
        Mockito.when(pickPc.defineWinner(pickPlayer)).thenReturn(GamePickEnum.GameResultEnum.WIN);

        gameCommand.execute(view);

        Mockito.verify(view).printMessage("Computer wins");
    }

    @Test
    public void executeShouldCallViewPrintPlayerWinsMessage(){
        Mockito.when(pickPc.defineWinner(pickPlayer)).thenReturn(GamePickEnum.GameResultEnum.LOOSE);

        gameCommand.execute(view);

        Mockito.verify(view).printMessage("Player wins");
    }
}
