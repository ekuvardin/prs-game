package junit.command;

import command.QuitCommand;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import view.IView;

public class QuitCommandTest {

    @Mock
    IView view;

    QuitCommand quitCommand;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
        quitCommand = new QuitCommand();
    }

    @Test
    public void executeShouldCallPrintMessage(){
        quitCommand.execute(view);

        Mockito.verify(view).printMessage("The program will be terminated");
    }
}
